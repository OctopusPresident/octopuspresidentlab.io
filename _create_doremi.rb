require 'open-uri'
require 'fileutils'

FileUtils.rm_rf('_posts')
FileUtils.rm_rf('assets')
FileUtils.mkdir('_posts')
FileUtils.mkdir_p('assets/img')

images = {}

tag_hashes =
{
  'doremi1' => 'Ojamajo Doremi 16' ,
  'doremi2' => 'Ojamajo Doremi 16 - Naive' ,
  'doremi3' => 'Ojamajo Doremi 16 - Turning Point' ,
  'doremi4' => 'Ojamajo Doremi 17' ,
  'doremi5' => 'Ojamajo Doremi 17 2nd ~Kizashi~' ,
  'doremi6' => 'Ojamajo Doremi 17 3rd ~Come On!~' ,
  'doremi7' => 'Ojamajo Doremi 18' ,
  'doremi8' => '\'Ojamajo Doremi 18 2nd: Spring has...\'' ,
  'doremi9' => 'Ojamajo Doremi 19' ,
  'doremi10' => 'Ojamajo Doremi 20\'s'
}

Dir.glob("_doremi/*/*.markdown") do |filename|
  # Skip the file if it is '.', '..', or ends with '1.markdown'.
  next if filename == '.' or filename == '..' or filename.end_with?('1.markdown')

  # Get the date of the file from the Git log.
  date = %x[git log --reverse --pretty="format:\%ci" #{filename} | head -1]

  # Get the tag from the directory name.
  tag = File.dirname(filename).tr('_/', '')

  # Add the image count.
  if images[tag].nil?
    images[tag] = 0
  end

  # Read the contents of the file into the 'complete_file' variable.
  complete_file = IO.read(filename)

  # Extract the title from the first line of the file.
  title = complete_file.lines.first[2..-1].delete("\n")

  # Extract the content of the file, excluding the first two lines.
  content = complete_file.lines[2..-1].join

  # Define a regular expression to match image URLs in the content.
  img_regex = /!\[img\]\((.*?)\)/

  # Find all image URLs in the content and download them to the 'assets/img' directory.
  content.scan(img_regex).flatten.each do |url|
    data = URI.open(url).read
    result = "assets/img/#{tag}_#{images[tag] += 1}.png"
    File.open(result, "wb") { |f| f.write(data) }
    new_url = "/" + result
    content.gsub!(url, new_url)
  end

  # Construct the front matter for the Jekyll post.
  front_matter = "---\n"
  front_matter += "layout: post\n"
  front_matter += "title: #{title}\n"
  front_matter += "date: #{date}\n"
  front_matter += "categories: doremi\n"
  front_matter += "tags:\n  - #{tag_hashes[tag]}\n---\n"

  # Write the Jekyll post to a file in the '_posts' directory.
  File.open("_posts/#{date.split[0]}-#{title.downcase.gsub(/[^a-z0-9]+/, '-').gsub(/^-+|-+$/, '')}.markdown", "wb") { |f| f.write(front_matter + content + "\n{% include comment.html %}\n") }

end
