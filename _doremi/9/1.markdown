# Illustrations

<style>
    figure {
        text-align: center;
    }

    figcaption {
        display: block;
        margin: 0 auto;
        text-align: center;
    }
</style>

<figure>
    <img src="https://static.wikia.nocookie.net/ojamajowitchling/images/2/28/DLN9_1.png/revision/latest/scale-to-width-down/1000" alt="img"/>
</figure>

<figure>
    <img src="https://static.wikia.nocookie.net/ojamajowitchling/images/c/c8/DLN9_2.png/revision/latest/scale-to-width-down/1000" alt="img"/>
</figure>

<figure>
    <img src="https://static.wikia.nocookie.net/ojamajowitchling/images/d/d4/DLN9_3.png/revision/latest/scale-to-width-down/1000" alt="img"/>
</figure>

<figure>
    <img src="https://static.wikia.nocookie.net/ojamajowitchling/images/1/1c/DLN9_4.png/revision/latest/scale-to-width-down/1000" alt="img"/>
</figure>

<figure>
    <img src="https://static.wikia.nocookie.net/ojamajowitchling/images/f/f8/DLN9_5.png/revision/latest/scale-to-width-down/1000" alt="img"/>
</figure>

<figure>
    <img src="https://static.wikia.nocookie.net/ojamajowitchling/images/9/92/DLN9_6.png/revision/latest/scale-to-width-down/1000" alt="img"/>
</figure>

<figure>
    <img src="https://static.wikia.nocookie.net/ojamajowitchling/images/0/05/DLN9_7.png/revision/latest/scale-to-width-down/1000" alt="img"/>
</figure>

<figure>
    <img src="https://static.wikia.nocookie.net/ojamajowitchling/images/f/f3/DLN9_8.png/revision/latest/scale-to-width-down/1000" alt="img"/>
    <figcaption>We'll always be together, forever and ever.</figcaption>
</figure>
