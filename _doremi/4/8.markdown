# Interview with Shishido Rumi (Segawa Onpu's seiyuu)

<br/><br/>

──What did you think when you heard that the members of "Ojamajo Doremi" would appear as 16-year-olds?

My honest impression was "finally!" It was a great work, so I thought it would make some sort of comeback. But it's an unfounded confidence (laughs).

<br/><br/>

──What are your thoughts after reading it?

At first, I couldn't get my hands on the first volume anywhere. I went to about five bookstores, but ended up buying it online. I wondered what happened because Onpu-chan wasn't there at first. But in the end, it didn't disappoint. I thought, "Ah, this is how it turns out."

I think there were some amazing developments for Onpu-chan that weren't depicted in the manuscript. I'm looking forward to seeing what kind of wonderful woman she'll become. Even now, as a close friend of the MAHO-do members, she has a slightly unique feeling, but she'll probably become even cooler, won't she?

<br/><br/>

──What was the audition like for the TV series?

I auditioned for the role of Doremi-chan at first. Then I only received a message saying "you didn't make it." After a while, I was offered the role of Onpu-chan without an audition. I later heard from the staff that when I auditioned for the role of Doremi-chan, they decided to give me the role of Onpu-chan. Maybe it was because I was an idol at that time.

<br/><br/>

──By the way, I heard that the first title idea for the show was "Ojamajo Onpu" (Magical Witch Onpu), right!?

It seems that Harukaze Onpu was Doremi-chan's original name. I heard that it became the current title for various reasons.

<br/><br/>

──Next time, I'll ask Producer Seki about it (laughs). How was the dubbing experience?

Since the crew had been working together since "Neighborhood Story", the atmosphere on the set was really easy to work with. It felt like, "Ah, we're done by 8:30 a.m. on Sunday" (laughs). The sound staff said that both Chiba-san (Doremi's voice actress) and Akiya-san (Hazuki's voice actress) were not yet accustomed to the voice acting world at that time, so I was able to serve as a good intermediary for them as someone who had experience.

<br/><br/>

──Chiba-san (Doremi's voice actress) said that she had difficulty pronouncing the magic spells, but how was it with Onpu-chan's spells?

Hmm, maybe because I had been doing music for so long, I didn't struggle too much. First we got the spell words, then they added the melody afterwards. But I did try to deliberately shift away from normal tones a bit.

<br/><br/>

──Do you have any memorable episodes from the dubbing sessions or events at that time?

Onpu-chan was also a member of MAHO-do, but because she had a cool position, I remember preparing for her character by sitting a little away from everyone else and taking a step back to create that atmosphere during the recording.

Onpu-chan's character was a "child idol", so she had more solo songs than the other cast members. So I was called to events alone several times... But there were also quite a few occasions when I had to sing upbeat songs alone that were originally sung by everyone, and it was hard to come up with choreography by myself (laughs).

I was also invited to events in Germany and France. In Germany, Onpu-chan's name was "Nicola". When it came to autograph sessions, there were lines for three hours and everyone seemed so happy to say "Nicola, Nicola".

<br/><br/>

──What were you like at 16-17 years old, Shishido-san?

It was exactly when I debuted in the entertainment industry at 16 years old. I had wanted to work in the entertainment industry and be an actress since I was a child. I didn't have any special training like stage practices, but I was selected out of 80,000 people. But because I was from Hiroshima, I had a dialect and at first I couldn't speak at all, I just smiled. But the dialect naturally straightened itself out... It still comes out occasionally now, and it slipped out a bit during the recording of the drama CD this time too (laughs).

<br/><br/>

──Now, the drama CD (limited edition only) will be included. How was the recording for the CD?

Well, it was tough! I think Onpu-chan has experienced various unseen hardships and sufferings that weren't depicted in the manuscript (such as her mother's situation and retirement from the entertainment industry). Moreover, her first line in the drama CD starts directly with dialogue, without any narration or description leading into it, so getting the right mix of coolness, passion, experience and hardship was really tough. Since she had been a mature character since elementary school, it was even harder.

In the dubbing booth, I was always nervous while listening to Doremi-chan and others' conversations before entering.

<br/><br/>

──Finally, could you please give a message to the readers?

For readers who were excited when they first saw the cover of this series, hearing the voices will leave you wanting more, so I really hope it gets turned into an anime. So please buy a lot of copies (laughs). It's perfect for getting extra copies for yourself, your friends, and as presents for people overseas (laughs)! Thanks for your continued support.

<br/><br/><br/>

<div style="text-align: right">May 2013 At Toei Animation</div>
