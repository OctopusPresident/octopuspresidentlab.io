require 'jekyll'

Jekyll::Hooks.register :posts, :post_render do |post|
    # Use color emoji for hearts
    post.output.gsub!("♥", "<span style=\"font-style: normal; display: inline-flex; vertical-align: baseline; font-family: 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji';\">♥</span>")
end
