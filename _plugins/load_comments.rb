require 'jekyll'
require 'jekyll/converters/markdown'
require 'json'
require 'rest-client'

$comments_data = {}

def load_comments_data(site, token)
    project_id = site.config['gitlab_project_id']
    label = "blog_comments"
    gitlab_api_url = "https://gitlab.com/api/v4/projects/#{project_id}/issues?labels=#{label}"

    response = RestClient.get(gitlab_api_url, { "PRIVATE-TOKEN" => token })
    issues = JSON.parse(response)

    title_key = ""
    issues.each do |issue|
        title_key = issue["title"]
        notes_url = "#{issue["_links"]["notes"]}?sort=asc&order_by=updated_at"
        notes_response = RestClient.get(notes_url, { "PRIVATE-TOKEN" => token })
        notes = JSON.parse(notes_response)

        comment = '
<style>
  .comment-margin * {
    margin-top: 0;
    margin-bottom: 5px;
  }
</style>
' + "\n<div class=\"comment-margin\"><p>Click <a href=\"#{issue["web_url"]}\">here</a> to leave comments on GitLab.</p></div>\n"
        # Output the JSON data for each comment with author and avatar URL
        notes.each do |note|
            name = note['author']['name']
            comment_body = note['body']
            avatar = note['author']['avatar_url']
            begin
                # Comment made on the website directly
                json_object = JSON.parse(comment_body)
                name = json_object['name']
                comment_body = json_object['comment']
                if not json_object['gravatar'].empty?
                    avatar = "https://www.gravatar.com/avatar/#{json_object['gravatar']}?size=50"
                end
            rescue JSON::ParserError => e
            end
            markdown_renderer = Jekyll::Converters::Markdown.new(site.config)
            comment_body = markdown_renderer.convert(comment_body)
            comment << <<-HTML
<div style="display: flex; align-items: center; margin-bottom: 10px;">
  <div style="margin-right: 10px; display: contents;">
    <img src="#{avatar}" style="border-radius: 50%; width: 50px; height: 50px; align-self: flex-start; margin-right: 10px;">
  </div>
  <div>
    <div style="font-weight: bold; margin-bottom: 5px;">#{name}</div>
    <div style="background-color: #f0f0f0; padding: 10px; border-radius: 10px;">
      <div class="comment-margin">
        #{comment_body}
      </div>
    </div>
  </div>
</div>
HTML
            end
        $comments_data[title_key] = comment
    end
end

Jekyll::Hooks.register :site, :after_init do |site|
    token = site.config['gitlab_token']
    if not token.nil?
        load_comments_data(site, token)
    end
end

Jekyll::Hooks.register :posts, :pre_render do |posts|
    posts.site.data['comments_data'] = $comments_data
end
